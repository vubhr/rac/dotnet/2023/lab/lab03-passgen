using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace PassGen.Pages
{
    public class CheckModel : PageModel
    {
        public string Message { get; set; }
        public void OnGet(string password)
        {
            if (Passwords.Check(password)) {
                Message = "Password is correct";
            } else {
                Message = "Password is incorrect";
            }

            // FAT MODEL - SLIM CONTROLLER

        }
    }
}
